require('./testHelpers');
const { assert, expect } = require('chai');
const service = require('../src/db/logging.service');
const LogMessage = require('../src/db/logging.model');
const msg = require('./mockData');
describe('Log messages service', () => {
  describe('Save and get all log messages', done => {
    it('save and return all mocked log messages from repository', async () => {
      await LogMessage(msg[0]).save({});
      await LogMessage(msg[1]).save({});
      service
        .get({})
        .then(result => {
          expect(result.length).to.equal(msg.length);
        })
        .catch(err => {
          assert.fail('expected', 'actual', err);
          // assert.fail('actual', err);
        })
        .finally(done);
    });
  });
});
