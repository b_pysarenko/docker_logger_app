const mongoose = require('mongoose');
const {db} = require('../src/config/index');

mongoose.Promise = global.Promise;
mongoose.connect(db.url, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  user: db.user,
  pass: db.user,
  dbName: db.dbName
});
mongoose.connection
  .once('open', () => console.log('Connected!'))
  .on('error', error => {
    console.warn('Error : ', error);
  });

beforeEach(done => {
  mongoose.connection.collections.logmessages.drop(() => {
    done();
  });
});

after(done => {
  mongoose.disconnect(() => {
    done();
  });
});
