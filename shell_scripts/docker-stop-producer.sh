#-f "status=exited"
RUNNING_IDS=$(docker ps -aqf ancestor='logs-producer:latest' -f 'status=running')
echo "stopped:"
docker stop $RUNNING_IDS
IDS=$(docker ps -aqf "ancestor=logs-producer:latest")
echo "removed:"
docker rm $IDS
