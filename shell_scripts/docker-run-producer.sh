#!/usr/bin/env bash
MAX=6
echo "running:"
for ((i = 1; i <= MAX; i++)); do
  docker run -d --name "producer$i" --label "label=producer$i" logs-producer:latest
  sleep 3;
done

#sleep 30;
#echo "stopping:"
#for ((i = 1; i <= MAX/2; i++)); do
#  docker stop $(docker ps -a -q -f "name=producer$i")
#  #sleep 1;
#done

