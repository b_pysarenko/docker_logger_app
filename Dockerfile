FROM node:10.17.0-alpine

ARG APP_DIR=/usr/src/app
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}


COPY package.json ${APP_DIR}
RUN npm install --production

COPY . ${APP_DIR}

ARG PORT=3000
#ENV PORT ${PORT}
EXPOSE ${PORT}

CMD [ "npm","run", "start-dev" ]