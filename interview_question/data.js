const HashMap = require('hashmap');

class Line {
  constructor(id, start, end) {
    this.id = id;
    this.start = start;
    this.end = end;
    this.worked = new HashMap();
  }

  saveAsNearest(line) {
    if (
      this.id !== line.id &&
      this.start >= line.start &&
      this.start < line.end
    ) {
      this.worked.set(line.id, line);
    }
  }

  totalNearest() {
    return this.worked.size;
  }
}

const inputData = [
  new Line('A', 1, 5),
  new Line('B', 2, 16),
  new Line('C', 4, 6),
  new Line('D', 7, 10),
  new Line('E', 12, 14),
  new Line('F', 3, 9),
  new Line('G', 11, 13)
];

module.exports.inputData = inputData;