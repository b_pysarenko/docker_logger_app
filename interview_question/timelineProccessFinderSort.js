const inputData = require('./data').inputData;

const getPoints = (data) =>{
  let total = [];
  data.map((item)=>{
    total.push({type:'start', val:item.start});
    total.push({type:'end', val:item.end});
  });
  return total;
};

const sortFunction = async () => {
  let arr = await getPoints(inputData);
  console.time('sort');
  await arr.sort((a,b)=>{
    return a.val-b.val
  });
  let counter = 0;
  let max = 0;
  for (var i = 0; i < arr.length; i++){
    if ('start'=== arr[i].type){
      counter++;
      if (counter > max) max = counter;
    } else if ('end'=== arr[i].type){
      counter--;
    }
    console.log(counter);
  }
  console.log('total ' + max);
  console.timeEnd('sort');
};

sortFunction();
