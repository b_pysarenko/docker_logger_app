const inputData = require('./data').inputData;

const result = [];
let maxProcess = null;

const graphBuilder = async list => {
  console.time('graph');
  await list.forEach(item => {
    new Promise((resolve) => {
      for (const i in inputData) {
        item.saveAsNearest(inputData[i]);
      }
      result.push(item);
      resolve(item);
    }).then(obj => {
      const total = obj.totalNearest() + 1;
      if (total > maxProcess) maxProcess = total;
      console.log(total);
    });
  });
  console.log('total ' + maxProcess);
  console.timeEnd('graph');
};

graphBuilder(inputData);
