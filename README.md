# Docker logs collector app

### Workspace requirements
    - Installed Docker Comunity Edition + Docer Compose(for deployment)
    - Access to internet (for pulling docker images)
    - Installed curl with python json.tool (instead can be used any REST client)
#### 1st step - Deployment
Execute script "run.sh" and wait while our apps will run. 
After deployment on the local Docker host will be run two main containers: MongoDB and LogsCollector applications. 
In addition, there will be run (sequentially with three-second timeshift ) six auxiliary containers, which will produce log records. 
Our app will attach to part of them depend on the listening strategy.  
####2nd step - Test
Execute script "run-curl.sh" and check response. 
We can place request conditions in the body as JSON using MongoDB querying format. 
Also, we can use any other REST client.
#### 3rd step - Rollback
Execute script "stop.sh" and wait while containers and images will be removed from docker.
