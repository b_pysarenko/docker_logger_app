const LogMessage = require('./logging.model');

module.exports = {
  save(msg) {
    LogMessage(msg).save({});
  },

  get(query) {
    return LogMessage.find(query,{_id:0,});
  }
};
