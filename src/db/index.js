const mongoose = require('mongoose');
const log = require('../config/logger');

async function initConnection(cfg) {
  await mongoose
    .connect(cfg.url, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      user: cfg.user,
      pass: cfg.user,
      dbName: cfg.dbName
    })
    .then(() => {
      log.info(`Connected to: ${db.host}`);
    })
    .catch(err => log.error(`_Connection error: ${err.message}`));

  const db = mongoose.connection;
  db.once('open', () => log.info(`Open connection: ${db.host}`)).on(
    'error',
    error => log.error(`Error ${error.message}`)
  );
}

module.exports = initConnection;
