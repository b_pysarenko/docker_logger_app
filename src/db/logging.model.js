const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    ID: {
      type: String,
      required: true
    },
    Attributes: {
      type: Object
    },
    text: {
      type: String,
    },
    time: {
      type: Date,
      default: Date.now
    }
  },
  { versionKey: false }
);

module.exports = mongoose.model('LogMessage', schema);