require('dotenv').config({path: "../../.env"});
const env = process.env.NODE_ENV || 'local';

const config = {
  develop: {
    server: {
      port: 3000,
      docker: {
        socketPath: '/var/run/docker.sock'
      }
    },
    db: {
      url: 'mongodb://db_server:27017/LoggedMessages',
      user: 'dockerLoggingService',
      dbName: 'LoggedMessages'
    }
  },
  local: {
    server: {
      port: 4000,
      docker: {
        socketPath: '/var/run/docker.sock'
      }
    },
    db: {
      url: 'mongodb://localhost:27007/LoggedMessages',
      user: 'dockerLoggingService',
      dbName: 'LoggedMessages'
    }
  }
};

module.exports = Object.assign(config[env],{ env: env});
