/*when 'onlyLabels' field is true app will record only containers which labels is present in 'labels' array,
otherwise will be recorded  all containers except which labels present in 'labels' array*/
const strategy = {
  onlyLabels: true,
  labels:[
    'producer1',
    'producer3',
    'producer5',
  ]
};

module.exports = strategy;