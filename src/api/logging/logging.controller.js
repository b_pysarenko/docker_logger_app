const logService = require('../../dao');

class LoggingController {
  static getLogs(req, res) {
    logService.getRecords(req.body).then(data => {
      return res
        .status(200)
        .json(data.length ? data : { result: 'found  0 objects' });
    });
  }
}

module.exports = LoggingController;
