const router = require('express').Router();
const controller = require('./logging.controller');

router.get('/logs', controller.getLogs);

module.exports = router;
