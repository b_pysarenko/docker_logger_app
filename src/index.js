const express = require('express');
const error = require('http-errors');
const log = require('./config/logger');
const { env, server, db } = require('./config');
const route = require('./api/logging');
const errorHandler = require('./api/error/error.middleware');
const docker = require('./docker');
require('./db')(db);


const app = express();

app.use(express.json());
app.use('/api', route);
app.use(function (req, res, next) {
  next(error(404));
});
app.use(errorHandler);
app.listen(server.port, () => log.info(`Service started on : ${server.port}`));
log.info(`Run in '${env}' environment`);



module.exports = app;
