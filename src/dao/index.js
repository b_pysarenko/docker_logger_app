const Factory = require('./storageFactory');

module.exports = Factory.getStorage('db');