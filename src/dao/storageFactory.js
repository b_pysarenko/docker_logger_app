const Service = require('./storageService');
const db = require('../db/logging.service');
const services = { db };

module.exports = {
  getStorage(type) {
    if (!services[type])
      throw 'unknown or not implemented data layer type, please use "db" layer';
    const storage = services[type];
    return new Service(storage);
  }
};
