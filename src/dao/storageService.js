
class StorageService{
  constructor(storage){
    this.storage = storage;
  }

  saveNewRecord(data) {
    this.storage.save(data);
  }

  getRecords(query) {
    return this.storage.get(query);
  }
}

module.exports = StorageService;