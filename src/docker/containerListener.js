var stream = require('stream');
const dao = require('../dao');


const attachContainer = async (docker, attr) => {
  const container = docker.getContainer(attr.ID);
  await container.inspect((err, data) => {
    const logStream = new stream.PassThrough();
    logStream.on('data', function(chunk) {
      const line = chunk.toString('utf8').trim();
      if (line) {
        attr['text'] = line;
        dao.saveNewRecord(attr);
      }
    });

    container.logs(
      {
        follow: true,
        stdout: true,
        stderr: true
      },
      function(err, stream) {
        if (err) {
          return log.error(err.message);
        }
        container.modem.demuxStream(stream, logStream, logStream);
        stream.on('end', function() {
          logStream.end(`Logging of container ${attr.ID} finished`);
        });
      }
    );
  });
};

module.exports = attachContainer;