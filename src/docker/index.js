const Docker = require('dockerode');
const docker = new Docker({ socketPath: '/var/run/docker.sock' });
const log = require('../config/logger');
const checker = require('../utils/filterChecker');
const listener = require('./containerListener');

docker.getEvents({}, function(err, data) {
  if (err) {
    log.error(err.message);
  } else {
    log.info(data);
    data.on('data', function(chunk) {
      const event = JSON.parse(chunk.toString('utf8'));
      if ('start' === event.status) {
        const attr = event.Actor;
        checker(attr)
          .then(valid => {
            if (valid) listener(docker, attr);
          })
          .catch(err => {
            log.error(err.message);
          });
      }
    });
  }
});

module.exports = docker;
