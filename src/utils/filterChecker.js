const {onlyLabels, labels} = require('../config/filterConfig');

const filter = (attr) => {
  return new Promise((resolve)=>{
    const inList = labels.indexOf(attr.Attributes.label)>-1;
    resolve (onlyLabels ? inList : !inList);
  })
};

module.exports = filter;