const winston = require('winston');

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [
    new winston.transports.Console({
      prettyPrint: true,
      timestamp: true,
      level: 'silly',
    }),
    new winston.transports.File({
      filename: '../logfile.log',
      level: 'silly',
      timestamp: true
    })
  ]
});

module.exports = logger;