const log = require('./logger');

const levels = ['error', 'warn', 'info', 'debug'];

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRndTime(){
  return Math.random() * 10000;
}

const initLog = () => {
  const i = getRandomInt(0,levels.length);
  const f = eval(`log.${levels[i]}`);
  const time = getRndTime();
  setTimeout(()=>{
    f(`Log message with timeshift - ${time}`);
  },time);
};

setInterval(initLog, 10000);


